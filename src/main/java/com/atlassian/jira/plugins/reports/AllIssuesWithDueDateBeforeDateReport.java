package com.atlassian.jira.plugins.reports;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.plugin.report.impl.AbstractReport;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ParameterUtils;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.google.common.collect.ImmutableMap;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;

import java.util.*;

@Scanned
public class AllIssuesWithDueDateBeforeDateReport extends AbstractReport {
    private static final Logger log = Logger.getLogger(AllIssuesWithDueDateBeforeDateReport.class);

    private final DateTimeFormatter formatter;
    @JiraImport
    private final SearchProvider searchProvider;
    @JiraImport
    private final ProjectManager projectManager;
    private Date dueDate;
    private Long projectId;
    private Date fromDate = new LocalDate(0).toDate();


    public AllIssuesWithDueDateBeforeDateReport(SearchProvider searchProvider, ProjectManager projectManager, @JiraImport DateTimeFormatterFactory dateTimeFormatterFactory) {
        this.formatter = dateTimeFormatterFactory.formatter().withStyle(DateTimeStyle.DATE).forLoggedInUser();
        this.searchProvider = searchProvider;
        this.projectManager = projectManager;
    }

    public void validate(ProjectActionSupport action, Map params) {
        try {
            String date = params.get("dueDate").toString();
            dueDate = (!date.isEmpty()) ? formatter.parse(date) : LocalDate.now().toDate();
        } catch (IllegalArgumentException e) {
            action.addError("dueDate", action.getText("report.duedatereport.duedate.required"));
            log.error("Exception while parsing dueDate");
        }
        projectId = ParameterUtils.getLongParam(params, "selectedProjectId");
        if (projectId == null || projectManager.getProjectObj(projectId) == null){
            action.addError("selectedProjectId", action.getText("report.duedatereport.projectid.invalid"));
            log.error("Invalid projectId");
        }
    }

    public String generateReportHtml(ProjectActionSupport action, Map params) throws Exception {
        List<Issue> issues = getAllIssues(action.getLoggedInUser());
        final Map paramsForView = ImmutableMap.builder()
                .put("dueDate", formatter.format(dueDate))
                .put("formatter", formatter)
                .put("issues", issues).build();
        return descriptor.getHtml("view", paramsForView);
    }

    private List<Issue> getAllIssues(ApplicationUser user) {
        JqlQueryBuilder builder = JqlQueryBuilder.newBuilder();
        builder.where().project(projectId).and().dueBetween(fromDate, dueDate);
        try {
            final SearchResults results = searchProvider.searchOverrideSecurity(builder.buildQuery(), user, PagerFilter.getUnlimitedFilter(), null);
            return results.getIssues();
        } catch (SearchException e1) {
            e1.printStackTrace();
            return null;
        }
    }

    public boolean showReport() {
        return isCurrentUserInGroup("project-manager");
    }

    private boolean isCurrentUserInGroup(String groupName) {
        ApplicationUser loggedInUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        return ComponentAccessor.getGroupManager().isUserInGroup(loggedInUser, groupName);
    }
}
